#define HELTEC_POWER_BUTTON
#include <heltec.h>
#include <WiFi.h>
#include <WebServer.h>

// Pause entre les paquets transmis en secondes
#define PAUSE               300
//fréquence d'émission de l'esp.
#define FREQUENCY           866.3   // pour l'Europe
//longueur d'onde de la bande passante en kHz
#define BANDWIDTH           250.0
#define SPREADING_FACTOR    9
//équivaut à 1mW de puissance pour l'antenne
#define TRANSMIT_POWER      0

const char *ssid = "FredIndustries"; // Nom du point d'accès
const char *password = "samsamsam"; // Mot de passe du point d'accès
WebServer server(80); //Port du serveur

//rxdata correspond au données récupérées par LoRa et pData permet de les transmettres au serveur
String rxdata;
String *pData = &rxdata;

String message;
String messageSent;
volatile bool rxFlag = false;

//variables de pause entre les messages envoyés
uint64_t last_tx = 0;
uint64_t tx_time;
uint64_t minimum_pause;


void setup() {

  heltec_setup();
  both.println("Radio init");
  RADIOLIB_OR_HALT(radio.begin());

  // établi la fonction de callback pour les paquets reçus
  radio.setDio1Action(rx);

  // établi les paramèters radio
  both.printf("Frequency: %.2f MHz\n", FREQUENCY);
  RADIOLIB_OR_HALT(radio.setFrequency(FREQUENCY));
  both.printf("Bandwidth: %.1f kHz\n", BANDWIDTH);
  RADIOLIB_OR_HALT(radio.setBandwidth(BANDWIDTH));
  both.printf("Spreading Factor: %i\n", SPREADING_FACTOR);
  RADIOLIB_OR_HALT(radio.setSpreadingFactor(SPREADING_FACTOR));
  both.printf("TX power: %i dBm\n", TRANSMIT_POWER);

  RADIOLIB_OR_HALT(radio.setOutputPower(TRANSMIT_POWER));

  // commence à capter
  RADIOLIB_OR_HALT(radio.startReceive(RADIOLIB_SX126X_RX_TIMEOUT_INF));
  delay(1000);
  Serial.println("\n");
  WiFi.persistent(false);
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);
  Serial.print("Tentative de connexion...");
  Serial.println(WiFi.softAPIP());
  Serial.println("\n");
  Serial.println("Connexion etablie!");
  Serial.print("Adresse IP: ");

  // Affiche l'adresse IP
  Serial.println(WiFi.localIP());  

  // Exprime les différente page de notre page web, Ici simplement notre page d'accueil et appelle la fonction handleroot
  server.on("/", handleRoot);  

  // Demarage du serveur
  server.begin();  

  if(WiFi.softAP(ssid, password)){
  IPAddress IP = WiFi.softAPIP();
  both.print("IP: ");
  both.println(IP);
  }

  server.on("/send", HTTP_POST, handleSend);
  server.on("/", HTTP_GET, handleRoot);

  //Démarrage du serv
  server.begin(80);

}

void loop() {

  //callback des fonctions du serveur
  server.handleClient();
  both.print(message);
  heltec_loop();

  bool tx_legal = millis() > last_tx + minimum_pause;
  bool *ptx = &tx_legal;

  // Transmet le paquet toutes les PAUSE secondes si le paquet n'est pas vide (il est vidé à tous les cycles)
  if ((PAUSE && tx_legal && millis() - last_tx > (PAUSE * 1000)) || message != "") {

    // Si l'utilisateur envoie un message avant la fin de la pause, lui d'attendre
    if (!tx_legal) {
      both.printf("limite légale, attendez %i sec.\n", ((minimum_pause - (millis() - last_tx)) / 1000) + 1);
      return;
    }

    both.printf("TX [%s] ", String(message).c_str());

    radio.clearDio1Action();
    heltec_led(50); // 50% luminosité

    tx_time = millis();

    RADIOLIB(radio.transmit(String(message).c_str()));
    tx_time = millis() - tx_time;

    heltec_led(0);

    if (_radiolib_status == RADIOLIB_ERR_NONE) {
      both.printf("OK (%i ms)\n", tx_time);
    } else {
      both.printf("échec (%i)\n", _radiolib_status);
    }

    // Maximum 1% duty cycle
    minimum_pause = tx_time * 100;

    last_tx = millis();
    radio.setDio1Action(rx);
    RADIOLIB_OR_HALT(radio.startReceive(RADIOLIB_SX126X_RX_TIMEOUT_INF));

    messageSent = message;
    message = "";

  }

  // Si un paquet est reçu, l'afficher avec le RSSI et SNR
  if (rxFlag) {

    rxFlag = false;
    radio.readData(rxdata);

    if (_radiolib_status == RADIOLIB_ERR_NONE) {
      both.printf("RX [%s]\n", rxdata.c_str());
      both.printf("  RSSI: %.2f dBm\n", radio.getRSSI());
      both.printf("  SNR: %.2f dB\n", radio.getSNR());
    }
    
    RADIOLIB_OR_HALT(radio.startReceive(RADIOLIB_SX126X_RX_TIMEOUT_INF));
  }

}

//code pas beau à changer (librairie)
void handleRoot() {

  String html = "<!DOCTYPE html>";
  html += "<html lang='fr'>";

  html += "<head>";

  html += "<meta charset='UTF-8'>";
  html += "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";

  html += "<title>AppliWeb</title>";


  html += "<style>";

  html += "body {";
    html += "font-family: Arial, sans-serif;";
    html += "margin: 0;";
    html += "padding: 0;";
    html += "background-color: #f9f9f9;";
  html += "}";

  html += ".container {";
    html += "padding: 1px;"; 
  html += "}";

  html += ".message {" ;
    html += "padding: 1px;";
    html += "margin: 5px 0; ";
    html += "border-radius: 20px;";
    html += "display: block;";
    html += "max-width: 50%;";
  html += "}";

  html += ".received {";
    html += "background-color: #e5e5ea;";
    html += "color: black;"; 
    html += "text-align: left;" ;
    html += "margin-left: 5px;";
    html += "margin-right: auto;";
  html += "}";

  html += ".sent {"; 
    html += "background-color: #007aff;"; 
    html += "color: white;"; 
    html += "text-align: right;"; 
    html += "margin-right: 5px;";
    html += "margin-left: auto;";
  html += "}";

  html += ".form-container {"; 
    html += "text-align: center;";
    html += "margin-top: 20px;";
  html += "}";

  html += "input[type='text'] {";
    html += "width: 70%;";
    html += "padding: 10px;";
    html += "margin-right: 10px;";
    html += "border-radius: 20px;";
    html += "border: 1px solid #ddd;";
    html += "margin-left:10px"; 
  html += "}";

  html += "button {";
    html += "padding: 10px 10px;";
    html += "border-radius: 20px;";
    html += "border: none;";
    html += "background-color: #007aff;";
    html += "color: white;";
    html += "margin-left:5px";
  html += "}";

  html += "</style>";


  html += "</head>";

  html += "<body>";

    html += "<div class='form-container'>";
      html += "<div class='container'>";
        html += "<div class='message sent'><h4>" + messageSent + "</h4></div>";
        html += "<div class='message received'><h4>" + String(*pData) + "</h4></div>";
      html += "</div>";
    html += "</div>";

    html += "<form action='/send' method='post'>";
      html += "<input type='text' id='message' name='message' placeholder='Écrire un message...'>";
      html += "<button type='submit'>Envoyer</button>";
    html += "</form>";

  html += "</body>";

  html += "</html>";

  server.send(200, "text/html", html);

}

void handleSend() {
  // Récupération du message envoyé via le formulaire
  message = server.arg("message");  // assignation du message à la var message
  
  server.send(200, "text/html", "<p>Message sent: " + message + "</p><a href='/'>Return</a>");  // Le message à bien été reçu affichage d'un lien de retour

}

void rx() {
  rxFlag = true;
}